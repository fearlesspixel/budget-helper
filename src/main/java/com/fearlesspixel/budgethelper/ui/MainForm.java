package com.fearlesspixel.budgethelper.ui;

import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * @author FearlessPixel
 * @since 5/14/2018
 */
public final class MainForm extends BorderPane {

    public MainForm() {
        super();

        final Text lblBanner = new Text("Budget Helper");
        lblBanner.setFont(Font.font(50));
        BorderPane.setAlignment(lblBanner, Pos.TOP_CENTER);

        setTop(lblBanner);
    }

}
