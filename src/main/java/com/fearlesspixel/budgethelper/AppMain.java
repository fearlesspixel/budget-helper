package com.fearlesspixel.budgethelper;

import com.fearlesspixel.budgethelper.ui.MainForm;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author FearlessPixel
 * @since 5/14/2018
 */
public final class AppMain extends Application {

    public void start(final Stage primaryStage) throws Exception {
        primaryStage.setTitle("Budget Helper");

        final Scene mainScene = new Scene(new MainForm(), 800, 750);

        primaryStage.setScene(mainScene);

        primaryStage.show();
    }

}
