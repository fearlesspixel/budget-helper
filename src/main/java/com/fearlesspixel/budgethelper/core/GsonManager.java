package com.fearlesspixel.budgethelper.core;

import com.google.gson.*;
import org.hildan.fxgson.FxGson;

import java.io.File;
import java.lang.reflect.Type;

/**
 * @author FearlessPixel
 * @since 5/14/2018
 */
public final class GsonManager {

    private static final GsonBuilder GSON_BUILDER = FxGson.coreBuilder().setPrettyPrinting()
            .serializeSpecialFloatingPointValues()
            .registerTypeAdapter(File.class, FileInstanceCreator.class);

    private static Gson GSON = GSON_BUILDER.create();

    /**
     * Adds a type adapter to the GSON Builder and re-creates the GSON instance
     *
     * @param type        the type definition for the type adapter being registered
     * @param typeAdapter This object must implement at least one of the {@link TypeAdapter},
     */
    public static void addTypeAdapter(final Type type, final Object typeAdapter) {
        GSON_BUILDER.registerTypeAdapter(type, typeAdapter);

        GSON = GSON_BUILDER.create();
    }

    /**
     * @return The GSON instance class
     */
    public static Gson getGSON() {
        return GSON;
    }


    public final class FileInstanceCreator implements JsonDeserializer<File>, JsonSerializer<File> {

        @Override
        public File deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            return new File(jsonElement.getAsString());
        }

        @Override
        public JsonElement serialize(File path, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(path.getAbsolutePath());
        }

    }

}
